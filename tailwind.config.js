module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue"
  ],
  theme: {
    extend: {
      fontFamily: {
        "lato": "'Lato', sans-serif",
        "merriweather": "'Merriweather', serif",
        "montserrat": "'Montserrat', sans-serif",
        "open-sans": "'Open Sans', sans-serif",
        "open-sans-condensed": "'Open Sans Condensed, sans-serif'",
        "oswald": "'Oswald', sans-serif",
        "roboto": "'Roboto', sans-serif",
        "roboto-slab": "'Roboto Slab', serif",
      }
    },
  },
  plugins: [],
}
