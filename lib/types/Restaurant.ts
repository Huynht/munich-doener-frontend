import { Thumbnail } from './Thumbnail'

export interface Restaurant {
    id: number
    name: string
    address: string
    x: number
    y: number
    thumbnail: Thumbnail
}
