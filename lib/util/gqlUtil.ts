export function parseObject(input) {
    const output = []
    for (const inputEntry of input) {
        const outputEntry: any = { id: inputEntry.id }
        for (const [inputKey, inputValue] of Object.entries(inputEntry.attributes)) {
            if ((<any>inputValue).data) {
                outputEntry[inputKey] = parseObject((<any>inputValue).data)
            } else {
                outputEntry[inputKey] = inputValue
            }
        }
        output.push(outputEntry)
    }
    if (output.length === 1) {
        return output[0]
    }
    return output
}

export function parseObjectList(input) {
    const output = parseObject(input)

    if (!Array.isArray(output)) {
        return [output]
    }
    return output
}