import { useRuntimeConfig } from "nuxt3"

export function imageUrl(inputUrl: string): string {
    // for develop env we need to prepend 'http://localhost:1337'
    if(inputUrl.startsWith('/')) {
        return 'http://localhost:1337' + inputUrl
    }
    return inputUrl
}