# TODO
- filter restaurants with
- finish restaurant schema
- finish cloudinary integration
- make header smaller

# SEO
- https://app.sistrix.com/en/serp-snippet-generator
- https://search.google.com/search-console
- https://www.sistrix.com/smart/

# Schemas

## RestaurantInformation
- Öffnungszeiten
- Preis von
  - Döner
  - Dürüm
  - Lahmacun mit Fleisch
- Baklava, Falafel, Haloumi
- sehr scharf möglich
- Fleischarten
  - Huhn
  - Lamm
  - Pute
- Gemüse
  - Blaukraut eingelegt
  - keine Petersilie im Gemüse
- Gemüsedöner ja/nein
- Selbstgebackenes Brot
- Vegane / Vegetarische Optionen
- Knoblauchsauce

## Review
- 30% Fleisch
- 25% Saucen
- 25% Brot  
- 20% Gemüse
  - Tomaten
  - Zwiebeln
  - Blaukraut
Bonus:
- 10% sonst. Gemüse, Schafskäse, Zitronensaft, etc.
- 5% Ambiente
